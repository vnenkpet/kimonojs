///<reference path="../CObject.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Kimono;
(function (Kimono) {
    (function (Controller) {
        /**
        * Base framework controller class
        */
        var BaseController = (function (_super) {
            __extends(BaseController, _super);
            function BaseController() {
                _super.apply(this, arguments);
            }
            return BaseController;
        })(Kimono.CObject);
        Controller.BaseController = BaseController;
    })(Kimono.Controller || (Kimono.Controller = {}));
    var Controller = Kimono.Controller;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=BaseController.js.map
