/// <reference path="Kimono.ts"/>
/**
* Base framework object class ***all classes should inherit from this***
*/
var Kimono;
(function (Kimono) {
    var Response = (function () {
        function Response() {
        }
        return Response;
    })();
    Kimono.Response = Response;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=Response.js.map
