var Kimono;
(function (Kimono) {
    /**
    * Base framework object class ***all classes should inherit from this***
    */
    var CObject = (function () {
        function CObject() {
        }
        CObject.prototype.getClassName = function () {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(this["constructor"].toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        return CObject;
    })();
    Kimono.CObject = CObject;
})(Kimono || (Kimono = {}));
/// <reference path="CObject.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Kimono;
(function (Kimono) {
    /**
    * Traditional RGBA color class
    */
    var Color = (function (_super) {
        __extends(Color, _super);
        function Color(r, g, b, a) {
            if (typeof a === "undefined") { a = 1; }
            _super.call(this);
            try  {
                this.setR(r);
                this.setG(g);
                this.setB(b);
                this.setA(a);
            } catch (e) {
                throw new Error("Invalid initialization");
            }
        }
        // setters:
        Color.prototype.setR = function (r) {
            if (r < 0 || r > 255)
                throw new Error("Invalid number");
            this.r = r;
        };

        Color.prototype.setG = function (g) {
            if (g < 0 || g > 255)
                throw new Error("Invalid number");
            this.g = g;
        };

        Color.prototype.setB = function (b) {
            if (b < 0 || b > 255)
                throw new Error("Invalid number");
            this.b = b;
        };

        Color.prototype.setA = function (a) {
            if (a < 0 || a > 255)
                throw new Error("Invalid alpha");
            this.a = a;
        };

        // TODO implement some kind of "toHexString"-like method and getters
        // static part
        Color.red = function () {
            return new Kimono.Color(255, 0, 0);
        };
        Color.green = function () {
            return new Kimono.Color(50, 255, 50);
        };
        Color.blue = function () {
            return new Kimono.Color(0, 0, 25);
        };
        Color.white = function () {
            return new Kimono.Color(255, 255, 255);
        };
        Color.black = function () {
            return new Kimono.Color(0, 0, 0);
        };
        return Color;
    })(Kimono.CObject);
    Kimono.Color = Color;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=Color.ts.js.map
