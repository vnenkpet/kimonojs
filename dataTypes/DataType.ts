/// <reference path="../CObject.ts"/>

module Kimono {

    /**
     * Data types for data mapping and other usages
     */
    export module DataType {

        export class BaseType extends Kimono.CObject {
            public value;
            constructor(value) {
                super();
                this.value = value;
            }
        }

        export class Int extends BaseType {
            public value : number;
        }

        export class Float extends BaseType {
            public value : number;
        }

        export class Varchar extends BaseType {
            public value : string;
        }

        export class Timestamp extends BaseType {
            public value : number;
        }
    }
}