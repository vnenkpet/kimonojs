/// <reference path="../CObject.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Kimono;
(function (Kimono) {
    /**
    * Data types for data mapping and other usages
    */
    (function (DataType) {
        var BaseType = (function (_super) {
            __extends(BaseType, _super);
            function BaseType(value) {
                _super.call(this);
                this.value = value;
            }
            return BaseType;
        })(Kimono.CObject);
        DataType.BaseType = BaseType;

        var Int = (function (_super) {
            __extends(Int, _super);
            function Int() {
                _super.apply(this, arguments);
            }
            return Int;
        })(BaseType);
        DataType.Int = Int;

        var Float = (function (_super) {
            __extends(Float, _super);
            function Float() {
                _super.apply(this, arguments);
            }
            return Float;
        })(BaseType);
        DataType.Float = Float;

        var Varchar = (function (_super) {
            __extends(Varchar, _super);
            function Varchar() {
                _super.apply(this, arguments);
            }
            return Varchar;
        })(BaseType);
        DataType.Varchar = Varchar;

        var Timestamp = (function (_super) {
            __extends(Timestamp, _super);
            function Timestamp() {
                _super.apply(this, arguments);
            }
            return Timestamp;
        })(BaseType);
        DataType.Timestamp = Timestamp;
    })(Kimono.DataType || (Kimono.DataType = {}));
    var DataType = Kimono.DataType;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=DataType.js.map
