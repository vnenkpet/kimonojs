/// <reference path="../CObject.ts"/>

module Kimono {

    export module Model {

        export class BaseEntity extends Kimono.CObject {

            private _properties = [];

            /**
             * gets properties of the model
             */
            public getProperties() {
                return this._properties;
            }
        }
    }

}