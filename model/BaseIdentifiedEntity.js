/// <reference path="../interfaces/IIdentifiedEntityInterface.ts"/>
/// <reference path="../dataTypes/DataType.ts"/>
/// <reference path="BaseEntity.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Kimono;
(function (Kimono) {
    (function (Model) {
        /**
        * Base identified entity class.
        */
        var BaseIdentifiedEntity = (function (_super) {
            __extends(BaseIdentifiedEntity, _super);
            function BaseIdentifiedEntity() {
                _super.apply(this, arguments);
            }
            // ID getter
            BaseIdentifiedEntity.prototype.getId = function () {
                return this.id.value;
            };
            return BaseIdentifiedEntity;
        })(Kimono.Model.BaseEntity);
        Model.BaseIdentifiedEntity = BaseIdentifiedEntity;
    })(Kimono.Model || (Kimono.Model = {}));
    var Model = Kimono.Model;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=BaseIdentifiedEntity.js.map
