/// <reference path="../CObject.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Kimono;
(function (Kimono) {
    (function (Model) {
        var BaseEntity = (function (_super) {
            __extends(BaseEntity, _super);
            function BaseEntity() {
                _super.apply(this, arguments);
                this._properties = [];
            }
            /**
            * gets properties of the model
            */
            BaseEntity.prototype.getProperties = function () {
                return this._properties;
            };
            return BaseEntity;
        })(Kimono.CObject);
        Model.BaseEntity = BaseEntity;
    })(Kimono.Model || (Kimono.Model = {}));
    var Model = Kimono.Model;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=BaseEntity.js.map
