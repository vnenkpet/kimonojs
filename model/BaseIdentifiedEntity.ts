/// <reference path="../interfaces/IIdentifiedEntityInterface.ts"/>
/// <reference path="../dataTypes/DataType.ts"/>
/// <reference path="BaseEntity.ts"/>

module Kimono {

    export module Model {

        /**
         * Base identified entity class.
         */
        export class BaseIdentifiedEntity extends Kimono.Model.BaseEntity implements Kimono.IIdentifiedEntityInterface {

            // persisted types
            private id : Kimono.DataType.Int;

            // ID getter
            getId():number {
                return this.id.value;
            }

        }
    }
}