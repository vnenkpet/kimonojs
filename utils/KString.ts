
module Kimono {

    export class KString {

        public capitalize(input : string): string {
            return input.charAt(0).toUpperCase() + input.slice(1);
        }

    }

}