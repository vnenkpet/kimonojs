var Kimono;
(function (Kimono) {
    var KString = (function () {
        function KString() {
        }
        KString.prototype.capitalize = function (input) {
            return input.charAt(0).toUpperCase() + input.slice(1);
        };
        return KString;
    })();
    Kimono.KString = KString;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=KString.js.map
