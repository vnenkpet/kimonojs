
module Kimono {

    /**
     * Returns a date time in typical YYYY-MM-DD hh:mm:ss format
     *
     * @returns {string}
     */
    export function getDateTime() : string {

        var date = new Date();
        var hour : number = date.getHours();
        var min : number = date.getMinutes();
        var sec : number = date.getSeconds();
        var year : number = date.getFullYear();
        var month : number = date.getMonth();
        var day : number  = date.getDate();

        var sHour = (hour < 10 ? "0" : "") + hour;

        var sMin : string = (min < 10 ? "0" : "") + min;
        var sSec : string = (sec < 10 ? "0" : "") + sec;
        var sMonth : string = (month < 10 ? "0" : "") + month;
        var sDay : string = (day < 10 ? "0" : "") + day;

        return year + "-" + sMonth + "-" + sDay + " " + sHour + ":" + sMin + ":" + sSec;
    }
}
