/// <reference path="CObject.ts"/>

module Kimono {

    /**
     * Traditional RGBA color class
     */
    export class Color extends Kimono.CObject {

        // private members:
        private r: number;
        private g: number;
        private b: number;
        private a: number;

        constructor(r : number, g : number, b : number, a : number = 1) {
            super();
            try {
                this.setR(r);
                this.setG(g);
                this.setB(b);
                this.setA(a);
            } catch (e) {
                throw new Error("Invalid initialization");
            }
        }

        // setters:
        public setR(r : number) {
            if(r < 0 || r > 255)
                throw new Error("Invalid number");
            this.r = r;
        }

        public setG(g : number) {
            if(g < 0 || g > 255)
                throw new Error("Invalid number");
            this.g = g;
        }

        public setB(b : number) {
            if(b < 0 || b > 255)
                throw new Error("Invalid number");
            this.b = b;
        }

        public setA(a : number) {
            if(a < 0 || a > 255)
                throw new Error("Invalid alpha");
            this.a = a;
        }

        // TODO implement some kind of "toHexString"-like method and getters

        // static part

        public static red() {
            return new Kimono.Color(255,0,0);
        }
        public static green() {
            return new Kimono.Color(50,255,50);
        }
        public static blue() {
            return new Kimono.Color(0,0,25);
        }
        public static white() {
            return new Kimono.Color(255,255,255);
        }
        public static black() {
            return new Kimono.Color(0,0,0);
        }
    }
}
