/// <reference path="Collections.ts"/>
/// <reference path="IArray2dInterface.ts"/>

module Kimono.Collections {

    /**
     * Class representing a scalable dynamic two-dimensional array.
     * @constructor
     */
    export class Array2d implements IArray2dInterface {

        private _values = [];
        private _size : number = 0;

        /**
         * Setter.
         *
         * @param x
         * @param y
         * @param value
         * @returns {boolean}
         */
        public set(x : number, y : number, value) {

            if (typeof this._values[x] == "undefined") {
                this._values[x] = [];
            }

            this._values[x][y] = value;
            this._size++;
            return true;
        }


        /**
         * Deletes values from the collection.
         *
         * @param x
         * @param y
         */
        public unset(x : number, y : number) {

            if (typeof this._values[x] !== "undefined") {
                if (typeof this._values[x][y] !== "undefined") {
                    this._values[x][y] = null;
                    this._size--;
                }
            }
        }

        /**
         * Getter.
         *
         * @param x
         * @param y
         * @returns {*}
         */
        public get(x : number, y : number) {

            if (typeof this._values[x] !== "undefined") {
                if (typeof this._values[x][y] !== "undefined") {
                    return this._values[x][y];
                }
            }
            else {
                return null;
            }

        }

        public getSize() {
            return this._size;
        }

    }

}
