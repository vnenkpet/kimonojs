/// <reference path="Collections.ts"/>

module Kimono.Collections {

    /**
     * Interface for scalable two-dimensional arrays
     * @constructor
     */
    export interface IArray2dInterface  {

        set(x : number, y : number, value);
        unset(x : number, y : number);
        get(x : number, y : number);
        getSize() : number;
    }

}
