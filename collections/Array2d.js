/// <reference path="Collections.ts"/>
/// <reference path="IArray2dInterface.ts"/>
var Kimono;
(function (Kimono) {
    (function (Collections) {
        /**
        * Class representing a scalable dynamic two-dimensional array.
        * @constructor
        */
        var Array2d = (function () {
            function Array2d() {
                this._values = [];
                this._size = 0;
            }
            /**
            * Setter.
            *
            * @param x
            * @param y
            * @param value
            * @returns {boolean}
            */
            Array2d.prototype.set = function (x, y, value) {
                if (typeof this._values[x] == "undefined") {
                    this._values[x] = [];
                }

                this._values[x][y] = value;
                this._size++;
                return true;
            };

            /**
            * Deletes values from the collection.
            *
            * @param x
            * @param y
            */
            Array2d.prototype.unset = function (x, y) {
                if (typeof this._values[x] !== "undefined") {
                    if (typeof this._values[x][y] !== "undefined") {
                        this._values[x][y] = null;
                        this._size--;
                    }
                }
            };

            /**
            * Getter.
            *
            * @param x
            * @param y
            * @returns {*}
            */
            Array2d.prototype.get = function (x, y) {
                if (typeof this._values[x] !== "undefined") {
                    if (typeof this._values[x][y] !== "undefined") {
                        return this._values[x][y];
                    }
                } else {
                    return null;
                }
            };

            Array2d.prototype.getSize = function () {
                return this._size;
            };
            return Array2d;
        })();
        Collections.Array2d = Array2d;
    })(Kimono.Collections || (Kimono.Collections = {}));
    var Collections = Kimono.Collections;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=Array2d.js.map
