var Kimono;
(function (Kimono) {
    /**
    * Base framework object class ***all classes should inherit from this***
    */
    var CObject = (function () {
        function CObject() {
        }
        CObject.prototype.getClassName = function () {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(this["constructor"].toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        return CObject;
    })();
    Kimono.CObject = CObject;
})(Kimono || (Kimono = {}));
//# sourceMappingURL=CObject.js.map
