
module Kimono {

    /**
     * Base framework object class ***all classes should inherit from this***
     */
    export class CObject {

        public getClassName():string {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(this["constructor"].toString());
            return (results && results.length > 1) ? results[1] : "";
        }

    }
}
