/**
 * Interface for ID-enabled entities
 */
module Kimono {

    export interface IIdentifiedEntityInterface {
        getId();
    }

}