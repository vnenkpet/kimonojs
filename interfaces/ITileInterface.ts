/// <reference path="../Color.ts"/>

/**
 * Interface for procedurally-generated tiles
 */
module Kimono {

    export interface ITileInterface {
        getPoint(x, y, z) : Color;
    }


}